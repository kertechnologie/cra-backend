package com.javainuse.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.javainuse.model.Projet;

@Repository
public interface ProjetRepository extends CrudRepository<Projet,Integer> {

	Projet save(Projet projet);
	List<Projet> findAll();
	void deleteById(int id); 
	Projet findById(int id);
}
