
package com.javainuse.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.javainuse.model.Candidat;

@Repository
public interface CandidatRepository extends CrudRepository<Candidat,Integer> {

	Candidat save(Candidat candidat);
	List<Candidat> findAll();
	void deleteById(int id); 
	Candidat findById(int id);
}
