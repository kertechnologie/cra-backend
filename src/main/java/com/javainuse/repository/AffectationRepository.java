package com.javainuse.repository;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.javainuse.model.Affectation;
import com.javainuse.model.Projet;
import com.javainuse.model.Client;
import com.javainuse.model.User;

@Repository
@Transactional
public interface AffectationRepository extends CrudRepository<Affectation,Integer> {

	Affectation save(Affectation affectation);
	List<Affectation> findAll();
	List<Affectation> findByUser(User user);
	void deleteById(int id); 
	Affectation findById(int id);
	Long deleteByUser (User user);
	Long deleteByProjet(Projet projet);
	//Long deleteByClient(Client Client);
	
	
	
}
