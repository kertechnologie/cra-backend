package com.javainuse.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import com.javainuse.model.Affectation;
import com.javainuse.model.Tache;

public interface TacheRepository extends CrudRepository<Tache,Integer> {
	Tache save(Tache tache);
	<S extends Tache> Iterable<S> saveAll(Iterable<S> taches);
	List<Tache> findAll();
	List<Tache> findByAffectation(Affectation affectation);
	void deleteById(int id); 
	Tache findById(int id);
	Long deleteByAffectation(Affectation affectation);
}
