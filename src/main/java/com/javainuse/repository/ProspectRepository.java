package com.javainuse.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.javainuse.model.Prospect;

public interface ProspectRepository extends CrudRepository<Prospect,Integer> {
	
	Prospect save(Prospect prospect);
	List<Prospect> findAll();
	void deleteById(int id); 
	Prospect findById(int id);
}
