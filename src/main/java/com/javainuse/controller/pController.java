package com.javainuse.controller;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.javainuse.model.Projet;
import com.javainuse.repository.AffectationRepository;
import com.javainuse.repository.ProjetRepository;

@RestController
@CrossOrigin(origins = "*")
public class pController {

	@Autowired
	ProjetRepository projetRepoistory;
	
	@Autowired AffectationRepository affectationRepository;

	@PostMapping("/sProjet")
	public Projet add(@RequestBody Projet projet) throws NoSuchAlgorithmException {

		return projetRepoistory.save(projet);

	}

	@GetMapping("/AllProject")
	public List<Projet> allProjects() {
		return projetRepoistory.findAll();
	}

	@GetMapping("/projet/{id}")
	public Projet show(@PathVariable String id) {
		int idprojet = Integer.parseInt(id);
		return projetRepoistory.findById(idprojet);
	}

	@GetMapping("/deleteProjet/{id}")
	public void delete(@PathVariable String id) {
		int idprojet = Integer.parseInt(id);
	    Projet projetTemp = projetRepoistory.findById(idprojet);
		affectationRepository.deleteByProjet(projetTemp);
		
		projetRepoistory.deleteById(idprojet);
	}

	@PostMapping("/updateProjet")
	public ResponseEntity<?> update(@RequestBody Projet projet) throws NoSuchAlgorithmException {

		if (projet.getIdprojet() == 0) {

			return ResponseEntity.badRequest().body("absence de l'id");
		}

		else {
			return ResponseEntity.ok(projetRepoistory.save(projet));
		}

	}
}
