package com.javainuse.model;


import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name="employe")
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idemp;
	
	@Column(name = "nom")
	private String nom;

	@Column(name = "prenom")
	private String prenom;
	
	@Column(name = "pseudo")
	private String pseudo; 
	
	public String getPseudo() {
		return pseudo;
	}

	@Column(name = "pwd")
	private String pwd; 
	
	@Column(name = "salt")
	private String salt; 
	
	@Column(name = "date_naissance")
	private String date_naissance; 
	
	@Column(name = "date_embauche")
	private String date_embauche; 
	
	@Column(name = "Contrat")
	private String contrat; 
	
	@Column(name = "email")
	private String email; 
	
	@Column(name = "role")
	private String role; 
	
	@Column(name = "telephone")
	private BigInteger telephone; 
	
	@Column(name = "adresse")
	private String adresse; 
	

	@Column(name = "salaire",nullable = true)
	private int salaire; 
	
	@Column(name = "num_secu")
	private BigInteger num_secu; 
      
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	List <Affectation> affectations;
	
	//@OneToMany(fetch = FetchType.LAZY, mappedBy = "user",cascade = CascadeType.ALL)
	//@JsonProperty(access = Access.WRITE_ONLY)
	//ist <Conges> conges;
	//private List<Conges> conges = null ;


	@PreRemove
	private void deleteAffectationsFromUser() {
		//System.out.println("preremove user");
	
		
	   affectations = null ;
	}


	
	public User(int idemp, String nom, String prenom, String pseudo, String pwd, String salt,String date_naissance, String date_embauche, String contrat,String email, String role,
			BigInteger telephone, String adresse, BigInteger num_secu, List <Affectation> affectations, /*List <Conges> conges*/ int salaire) {
		super();
		this.idemp = idemp;
		this.nom = nom;
		this.prenom = prenom;
		this.pseudo = pseudo;
		this.pwd = pwd;
		this.salt = salt;
		this.date_naissance = date_naissance;
		this.date_embauche = date_embauche;
		this.contrat = contrat;
		this.email = email;
		this.role = role;
		this.telephone= telephone;
		this.adresse = adresse;
		this.num_secu = num_secu; 
		this.affectations= affectations;
		//this.conges = conges;
		this.salaire = salaire;
						
	}

	public User() {
	}

	public int getIdemp() {
		return idemp;
	}

	public void setIdemp(int idemp) {
		this.idemp = idemp;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getPwd() {
		return pwd;
	}
	
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}
	
	public String getDate_naissance() {
		return date_naissance;
	}

	public void setDate_naissance(String date_naissance) {
		this.date_naissance = date_naissance;
	}

	public String getDate_embauche() {
		return date_embauche;
	}

	public void setDate_embauche(String date_embauche) {
		this.date_embauche = date_embauche;
	}

	public String getContrat() {
		return contrat;
	}

	public void setContrat(String contrat) {
		this.contrat = contrat;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public BigInteger getTelephone() {
		return telephone;
	}

	public void setTelephone(BigInteger telephone) {
		this.telephone = telephone;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public BigInteger getNum_secu() {
		return num_secu;
	}

	public void setNum_secu(BigInteger num_secu) {
		this.num_secu = num_secu;
	}
	
	public List<Affectation> getAffectations() {
		return affectations;
	}

	public void setAffectations(List<Affectation> affectations) {
		this.affectations = affectations;
	}

	/*public List<Conges> getConges() {
		return conges;
	}



	public void setConges(List<Conges> conges) {
		this.conges = conges;
	}
	*/
	public int getSalaire() {
		return salaire;
	}


	public void setSalaire(int salaire) {
		this.salaire = salaire;
	}



	@Override
	public String toString() {
		return "User [idemp=" + idemp + ", nom=" + nom + ", prenom=" + prenom + ", pseudo=" + pseudo + ", pwd=" + pwd
				+ ", salt=" + salt + ", date_naissance=" + date_embauche + ", date_embauche=" + date_embauche + ", contrat=" + contrat + ", email=" + email
				+ ", role=" + role + ", telephone=" + telephone + ", adresse=" + adresse + ", salaire=" + salaire
				+ ", num_secu=" + num_secu + ", affectations=" + affectations + ", /*conges= ]";
	}
	


	
}
