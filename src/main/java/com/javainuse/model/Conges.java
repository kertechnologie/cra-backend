package com.javainuse.model;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name="conge")
public class Conges {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idconge;
	String motif;
	String date_demande;
	String date_debut;
	String date_fin; 
	String description;
	String etat_demande; 
	
    @ManyToOne(fetch = FetchType.EAGER)   
    @JoinColumn(name = "idemp")
    //@OnDelete(action = OnDeleteAction.CASCADE) 
    User user;

	public Conges(int idconge, String motif, String date_demande, String date_debut, String date_fin,
			String description, String etat_demande, User user) {
		super();
		this.idconge = idconge;
		this.motif = motif;
		this.date_demande = date_demande;
		this.date_debut = date_debut;
		this.date_fin = date_fin;
		this.description = description;
		this.etat_demande = etat_demande;
		this.user = user;
	}

	public Conges() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public int getIdconge() {
		return idconge;
	}

	public void setIdconge(int idconge) {
		this.idconge = idconge;
	}

	public String getMotif() {
		return motif;
	}

	public void setMotif(String motif) {
		this.motif = motif;
	}

	public String getDate_demande() {
		return date_demande;
	}

	public void setDate_demande(String date_demande) {
		this.date_demande = date_demande;
	}

	public String getDate_debut() {
		return date_debut;
	}

	public void setDate_debut(String date_debut) {
		this.date_debut = date_debut;
	}

	public String getDate_fin() {
		return date_fin;
	}

	public void setDate_fin(String date_fin) {
		this.date_fin = date_fin;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEtat_demande() {
		return etat_demande;
	}

	public void setEtat_demande(String etat_demande) {
		this.etat_demande = etat_demande;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Conges [idconge=" + idconge + ", motif=" + motif + ", date_demande=" + date_demande + ", date_debut="
				+ date_debut + ", date_fin=" + date_fin + ", description=" + description + ", etat_demande="
				+ etat_demande + ", user=" + user.getIdemp() + "]";
	}
    
    

}