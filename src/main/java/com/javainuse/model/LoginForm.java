package com.javainuse.model;

public class LoginForm {

	 private String pseudo ;
	 private String pwd ;
     
	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	@Override
	public String toString() {
		return "LoginForm [pseudo=" + pseudo + ", pwd=" + pwd + "]";
	}

	public LoginForm(String pseudo, String pwd) {
		super();
		this.pseudo = pseudo;
		this.pwd = pwd;
	}

	public LoginForm() {
		super();
	}
     
	
     
     
}
