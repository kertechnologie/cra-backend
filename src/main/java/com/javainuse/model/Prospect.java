
package com.javainuse.model;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="prospect")
public class Prospect implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idprospect;
	
	@Column(name = "societe")
	private String societe;
	
	@Column(name = "interlocuteur")
	private String interlocuteur;
	
	@Column(name = "poste")
	private String poste;
	
	@Column(name = "tel")
	private String tel;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "adresse_postale_societe")
	private String adresse_postale_societe;
	
	public Prospect(int idprospect, String societe, String interlocuteur,String poste,String tel, String email, String adresse_postale_societe) {
		super();
		this.idprospect = idprospect;
		this.societe = societe;	
		this.interlocuteur = interlocuteur;
		this.poste = poste;
		this.tel = tel;
		this.email = email;
		this.adresse_postale_societe = adresse_postale_societe;
	}

	public Prospect() {
		super();
	}

	public int getIdprospect() {
		return idprospect;
	}

	public void setIdprospect(int idprospect) {
		this.idprospect = idprospect;
	}

	public String getSociete() {
		return societe;
	}

	public void setSociete(String societe) {
		this.societe = societe;
	}
	
	public String getInterlocuteur() {
		return interlocuteur;
	}

	public void setInterlocuteur(String interlocuteur) {
		this.interlocuteur = interlocuteur;
	}

	public String getPoste() {
		return poste;
	}

	public void setPoste(String poste) {
		this.poste = poste;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getAdresse_postale_societe() {
		return adresse_postale_societe;
	}

	public void setAdresse_postale_societe(String adresse_postale_societe) {
		this.adresse_postale_societe = adresse_postale_societe;
	}

	@Override
	public String toString() {
		return "Client [idprospect=" + idprospect + ", societe=" + societe + ", interlocuteur=" + interlocuteur
				+ ", poste=" + poste + ", tel=" + tel + ", email=" + email + ", adresse_postale_societe=" + adresse_postale_societe + "]";
	}






}
